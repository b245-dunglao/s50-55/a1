/*

to create a react app,

npx create-react-app project-name


to run our react application 
npm start


files to be removed:
SRC folder
App.test.js
index.css
reportWebVitals.js
logo.svg

we need to delete all the importations of the said files


package control: install package
add babel

npm install react-bootstrap bootstrap


the 'import' statement allows us to use the code/export modules from other files similar to how we use the "require" function in NodeJS

React JS it applies the concepts of rendering and mounting in order to display and create components

rendering refers to the process of calling/invoking a component returning set of instructions creating DOM

Mounting is when react JS renders or displays the component the initial DOM based on the instructions


Using the event.target.value will capture the value inputted by the user on our input area.

In the dependencies in useEffect
	1. Single dependency [dependency]
		on the initial load of the component the side effect / function will be triggered and whenever changes occur on our dependency.

	2. Empty dependecy []
		The sideEffect will only run during the initial load.

	3. Multiple dependency [dependency1, dependency2]
		The sideEffect will run during the initial load and whenever the state of the dependencies change. 



[SECTION] react-router-dom
	npm install react-router-dom

	For us to be able to use the module/library across all of our pages we have to contain them with BrowserRouter/Router

	Routes - we contain all of the routes in our react-app

	Route - specific route where in we will declare the url and also the component or page to be mounted


	We use Link if we want to go from one page to another page
	We use NavLink if we want to change our page using our NavBar
	

	The "localStorage.setItem" allows us to manipulate the browser's localStorage property to store information indefinitely. 
	

	[SECTION] Environment variable
		environtment variables are important in hiding the sensitive pieces of information like the backend API which can be exploited if added directly into our code. 

		

*/