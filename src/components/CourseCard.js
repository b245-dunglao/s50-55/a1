
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link} from 'react-router-dom';

import UserContext from '../UserContext.js';


export default function	CourseCard({courseProp}){


	const{_id, name,description, price } = courseProp;
	// The "course" in the CourseCard component is called a "prop" which is a shorthand for property

	// The curly braces are used for props to signify that we are providing information using expressions.

// Use the state hook for this component to be able to store its state.

// States are used to keep track of information related to individual components
	// const [getter, setter] = useState(initialGetterValue);

	const [enrollees, setEnrollees] = useState(0);
	const [seats, setSeats] = useState(30);

	// add new state that will declare whether the button is disable or not
	const [isDisabled, setIsDisabled] = useState(false);

	// initial value of enrollees state. 
	// console.log(enrollees);

	// if you want to change/reassign the value of the state
		// setEnrollees(1)

	const {user} = useContext(UserContext);

	function enroll(){

		if (seats > 1 && enrollees < 29){
			setEnrollees(enrollees+1);
			setSeats(seats-1)

		}
		else {
			
			alert(`Congratulations for taking the last seat for class: ${name}`);
			setEnrollees(enrollees+1);
			setSeats(seats-1)
			// document.querySelector(`#enroll-button-${id}`).setAttribute(`disabled`,true);
		}		
	}

	// Define a 'useEffect' hook to have the "CourseCard" component do perofmr a certain task. 
	// This will run automatically. 
	// Syntax: 
	// 	useEffect(sideEffect/function, [dependencies]);

	// sideEffect/function - it will run on the first load and will reload depending on the dependency array. 

	useEffect(()=>{
		if(seats === 0 ){
			setIsDisabled(true)
		}
	},[seats]);



	return (
			<Row className="mt-4">
				<Col>
					<Card>
					    <Card.Body>
							    <Card.Title>{name}</Card.Title>
							    <Card.Subtitle>Description:</Card.Subtitle>
							    <Card.Text>{description}</Card.Text>
							    <Card.Subtitle>Price:</Card.Subtitle>
							    <Card.Text>{price}</Card.Text>
							    <Card.Subtitle>Enrollees:</Card.Subtitle>
							    <Card.Text> {enrollees} </Card.Text>
							    <Card.Subtitle>Available Seats:</Card.Subtitle>
							    <Card.Text> {seats}</Card.Text>
							    {
							    	user?
							    	<Button as = {Link} to = {`/course/${_id}`} disabled={isDisabled}> See more details </Button>
							    	:
							    	<Button as = {Link} to ="/login" > Login </Button>
							    }
					    		

					    </Card.Body>
					 </Card>
				</Col>
			</Row>

		)}

	/*//id={`enroll-button-${id}`} */
	// onClick = {enroll}