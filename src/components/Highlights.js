import Card from 'react-bootstrap/Card';
import { Row, Col} from 'react-bootstrap';

export default function	Highlights(){
	return (
			<Row>
				<Col className = "col-md-4 col-10 m-md-0 m-1 mx-auto">
					<Card className= "cardHighlight">
					    <Card.Body>
					      <Card.Title>Learn from Home</Card.Title>
					      <Card.Text>
					          Some quick example text to build on the card title and make up the
					          bulk of the card's content.
					      </Card.Text>
					    </Card.Body>
					 </Card>
				</Col>
				<Col className = "col-md-4 col-10  m-md-0 m-1 mx-auto">	 
					 <Card className= "cardHighlight">
					     <Card.Body>
					       <Card.Title>Study Now, Pay Later</Card.Title>
					       <Card.Text>
					           Some quick example text to build on the card title and make up the
					           bulk of the card's content.
					       </Card.Text>
					     </Card.Body>
					  </Card>
				</Col>
				<Col className = "col-md-4 col-10  m-md-0 m-1 mx-auto">
					  <Card className= "cardHighlight">
					      <Card.Body>
					        <Card.Title>Be part of our community</Card.Title>
					        <Card.Text>
					            Some quick example text to build on the card title and make up the
					            bulk of the card's content.
					            
					        </Card.Text>
					      </Card.Body>
					   </Card>

				</Col>
			</Row>
		)
}


/*
<Card>
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
      </Card.Body>
    </Card>

*/