
import {Button, Form, Container, Row, Col} from 'react-bootstrap';
import {Fragment, useState, useContext, useEffect} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';



export default function Login () {
	const [email,setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	// const[user,setUser] = useState(localStorage.getItem('email'))

	const navigate = useNavigate();

	// Allows us to consume the UserContext object and it's properties for user validation

	const {user, setUser} = useContext(UserContext);

	// console.log(user);

	// Button State
	useEffect(() =>{
		
		if (email !== "" && password !== ""){
			setIsActive(false);
		}
		else{
			setIsActive(true);
		}
	}, [email,password])




	function login (event){
		// if you want to add the email of the autheticated user in the local storage
		// 		syntax: 
		//			localStorage.setItem("properName", value)

		event.preventDefault();

		// Process a fetch request to corresponding backend API
		// Syntax: fetch('url',{options});

		fetch(`${process.env.REACT_APP_API_URL}/user/login`,{
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email,
				password: password
			})
		}).then(result=>result.json())
		.then(data=>{
			console.log(data.auth);

			if(data === false){
				
				Swal.fire({
					title: "Authentication failed.",
					icon: "error",
					text: "Your credentials are wrong. You may register your account to our website."
				})
			}else{
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(localStorage.getItem('token'));

				//npm install sweetalert2
				//https://sweetalert2.github.io/

				Swal.fire({
					title: "Authentication successful!",
					icon: "success",
					text: "Welcome to Zuitt"
				})


				navigate('/');

			}
		})

		// localStorage.setItem("email",email);
		// setUser(localStorage.getItem('email'));

		
		// alert("Login success!");
		setEmail('');
		setPassword('');

		// navigate("/");

	}

	const retrieveUserDetails = (token) => {

		// The token sent as part of the request's header information

		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})

	}
	
	return (
		user ?
		<Navigate to = "/*"/>
		:
		
		<Fragment>
			<Row className="mt-5">
				<Col className="col-md-6 col-10 mx-auto p-3">
					<Container className="my-5">
					<h1 className ="text-center mt-5 text-muted"> Zuitt </h1>
					<h4 className ="text-center mt-2 text-muted">Opportunities for everyone, everywhere</h4>
					<Form className = "my-3" onSubmit ={event =>login(event)}>
					<h4 className ="text-left mt-2 text-muted">Login</h4>
					      <Form.Group className="mb-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control 
					        type="email" 
					        placeholder="Enter email" 
					        value ={email} 
					        onChange ={event=> setEmail(event.target.value)}
					        required/>
					      </Form.Group>
					      <Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Password</Form.Label>
					        <Form.Control 
					        type="password" 
					        placeholder="Password" 
					        value ={password} 
					        onChange ={event=> setPassword(event.target.value)}
					        required/>
					      </Form.Group>
					      <Form.Group className="mb-2">
					        <Form.Text href="" >
					          <a href="www.google.com/">Forgot Password?</a>
					        </Form.Text>
					      </Form.Group>
					      <Button variant="primary" type="submit" disabled={isActive}>
					        Sign In
					      </Button>
					    </Form>
					</Container>
				
				</Col>
			</Row>
		</Fragment>
		
	)
}

